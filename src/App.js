import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import AppLayout from "./components/AppLayout.js";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route component={AppLayout} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
