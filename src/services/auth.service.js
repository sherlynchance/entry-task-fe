import axios from "axios";

const { REACT_APP_API_URL } = process.env;

class AuthService {
  signIn(credentials) {
    return axios.post(`${REACT_APP_API_URL}login`, credentials).then((res) => {
      if (res.data.accessToken)
        localStorage.setItem("currentUser", JSON.stringify(res.data));

      return res.data;
    });
  }
}

export default new AuthService();
