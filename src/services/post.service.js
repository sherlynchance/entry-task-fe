import http from "../http.common";

class PostService {
  getAll() {
    return http.get(`posts`);
  }

  getPaginatedPosts(page) {
    return http.get(`/posts?_page=${page}&_limit=10`);
  }

  getPostById(id) {
    return http.get(`/posts/${id}`);
  }

  updatePost(id, data) {
    return http.post(`posts/${id}`, data);
  }

  getActionPosts() {
    return http.get(`post_actions`);
  }

  likePost(data) {
    return http.post(`post_actions`, data);
    // return http.post(`posts_actions?action_type=likes`, data);
    // http://localhost:8000/post_actions?isLike=true
  }

  unlikePost(id) {
    return http.delete(`post_actions/${id}`);
  }

  goingPost(data) {
    return http.post(`post_actions`, data);
  }

  notGoingPost(id) {
    return http.delete(`post_actions/${id}`);
  }
}

export default new PostService();
