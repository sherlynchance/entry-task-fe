import http from "../http.common";

class CommentService {
  getAll() {
    return http.get(`/comments`);
  }

  getCommentsByPostId(id) {
    return http.get(`/comments?post_id=${id}`);
  }

  postComment(comment) {
    return http.post("/comments", comment);
  }
}

export default new CommentService();
