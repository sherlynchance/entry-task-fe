// import { commentAction } from "./src/state";
// import * as actions from '../actions/posts/getPost';
// import { UPDATE_POST_SUCCESS } from '../actions/posts/updatePost';
import expect from "expect";
import { POST_COMMENT } from "./state/actionTypes/commentActionType";
import commentReducer, { initial_state } from "./state/reducers/commentReducer";
// import getPostMock from "../mocks/getPostMock";
// import { GET_COMMENTS } from "./src/state/actionTypes/commentActionType";
// import commentReducer from "./src/state/reducers/commentReducer";

// test('should return the initial state', () => {
//     expect(reducer(undefined, {})).toEqual([
//       {
//         text: 'Use Redux',
//         completed: false,
//         id: 0
//       }
//     ])
//   })

// console.log(initial_state)
describe("comments reducer", () => {
  it("should return the initial state", () => {
    expect(commentReducer([], {})).toEqual([]);
  });

  // it("should handle ADD_COMMENT"), () => {

  // };

  it("should handle POST_COMMENT", () => {
    const prevState = [
      {
        id: 1,
        comment: "Etiam tor gravida sem.",
        created_at: "10/31/2020",
        post_id: 15,
        user_id: 11,
      },
    ];
    expect(
      commentReducer(prevState, {
        type: POST_COMMENT,
        payload: {
          comment: "Etiam tor egravida sem.",
          created_at: "10/31/2020",
          post_id: 11,
          user_id: 11,
        },
      }),
    ).toEqual([
      ...prevState,
      {
        comment: "Etiam tor egravida sem.",
        created_at: "10/31/2020",
        post_id: 11,
        user_id: 11,
      },
    ]);
  });
});
