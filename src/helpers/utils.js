export function getChannelName(channels, channelId) {
  if (channels.length !== 0) {
    let channel = channels.find((channel) => channel.id === channelId);
    return channel.name;
  }
}

export function getUser(users, userId) {
  let user = users.find((user) => user.id === userId);
  return user;
}
