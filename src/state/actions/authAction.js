import AuthService from "../../services/auth.service";
import { SIGN_IN_SUCCESS, SIGN_IN_FAIL } from "../actionTypes/authActionType";

export const sign_in = (email, password) => (dispatch) => {
  return AuthService.signIn(email, password).then(
    (data) => {
      console.log(data);
      dispatch({
        type: SIGN_IN_SUCCESS,
        payload: data, // payload: {user: data}
      });

      return Promise.resolve();
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: SIGN_IN_FAIL,
      });

      dispatch({
        // type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    },
  );
};
