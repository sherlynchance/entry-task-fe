import { ChannelService } from "../../services";
import { GET_CHANNELS } from "../actionTypes/channelActionType";

export const getChannels = () => async (dispatch) => {
  try {
    const res = await ChannelService.getAll();

    dispatch({
      type: GET_CHANNELS,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (error) {
    return Promise.reject(error);
  }
};
