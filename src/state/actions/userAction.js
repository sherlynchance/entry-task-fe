import { UserService } from "../../services";
import { GET_USERS } from "../actionTypes/userActionType";

export const getUsers = () => async (dispatch) => {
  try {
    const res = await UserService.getUsers();

    dispatch({
      type: GET_USERS,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (error) {
    return Promise.reject(error);
  }
};
