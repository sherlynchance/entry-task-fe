import Moment from "moment";
import { extendMoment } from "moment-range";
import { PostService } from "../../services";
import {
  GET_POSTS,
  GET_PAGINATED_POSTS,
  FILTER_POSTS_BY_CHANNEL,
  RESET_ALL_STATE,
  LIKE_POST,
  UNLIKE_POST,
  GET_ACTION_POSTS,
  GET_POST_BY_ID,
  RESET_POST_FILTER,
  FILTER_LIKED_POSTS_BY_USER,
  FILTER_GOING_POSTS_BY_USER,
  FILTER_PAST_USER_ACTIVITES,
} from "../actionTypes/postActionType";

const moment = extendMoment(Moment);

export const getPosts = () => async (dispatch) => {
  try {
    const res = await PostService.getAll();

    dispatch({
      type: GET_POSTS,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const changePage = (page) => async (dispatch) => {
  try {
    const res = await PostService.getPaginatedPosts(page);

    dispatch({
      type: GET_PAGINATED_POSTS,
      payload: res.data,
    });
  } catch (error) {
    Promise.reject(error);
  }
};

export const getLikedPostsByUserId = (userId) => async (dispatch, getState) => {
  try {
    dispatch(getActionPosts());
    dispatch(getPosts()).then(() => {
      const posts = getState().posts;

      const likedPosts = posts.actionPosts.filter(
        (action) => action.user_id === userId && action.action_type === "likes",
      );

      let result = likedPosts.map((post) =>
        posts.allPosts.find((x) => x.id === post.post_id),
      );

      dispatch({
        type: FILTER_LIKED_POSTS_BY_USER,
        payload: result,
      });
    });
  } catch (error) {
    Promise.reject(error);
  }
};

export const getGoingPostsByUserId = (userId) => async (dispatch, getState) => {
  try {
    dispatch(getActionPosts());
    dispatch(getPosts()).then(() => {
      const posts = getState().posts;

      const likedPosts = posts.actionPosts.filter(
        (action) => action.user_id === userId && action.action_type === "going",
      );

      let result = likedPosts.map((post) =>
        posts.allPosts.find((x) => x.id === post.post_id),
      );

      dispatch({
        type: FILTER_GOING_POSTS_BY_USER,
        payload: result,
      });
    });
  } catch (error) {
    Promise.reject(error);
  }
};

export const getPastUserActivities = (userId) => async (dispatch, getState) => {
  try {
    dispatch(getActionPosts());
    dispatch(getPosts()).then(() => {
      const posts = getState().posts;

      const pastActions = posts.actionPosts.filter(
        (action) => action.user_id === userId,
      );

      let pastActionPosts = pastActions.map((post) =>
        posts.allPosts.find((x) => x.id === post.post_id),
      );

      let result = pastActionPosts.filter(
        (post) => post.date_end < new Date().toISOString(),
      );

      let uniqResult = [...new Set(result)];

      dispatch({
        type: FILTER_PAST_USER_ACTIVITES,
        payload: uniqResult,
      });
    });
  } catch (error) {
    Promise.reject(error);
  }
};

export const getPostById = (id) => async (dispatch) => {
  try {
    const res = await PostService.getPostById(id);

    dispatch({
      type: GET_POST_BY_ID,
      payload: {
        loading: false,
        post: res.data,
      },
    });

    return Promise.resolve(res.data);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const filterByChannel = (allPosts, filters) => async (dispatch) => {
  function filterDate() {
    switch (filters.date) {
      case "ANYTIME":
        return allPosts;

      case "TODAY":
        return allPosts.filter((post) =>
          moment().startOf("day").isSame(post.date_start, "date"),
        );

      case "TOMORROW":
        let tomorrow = moment().add(1, "day");
        return allPosts.filter((post) =>
          tomorrow.isBetween(post.date_start, post.date_end),
        );

      case "THIS WEEK":
        return allPosts.filter(
          (post) =>
            moment(post.date_start).isSame(new Date(), "week") ||
            moment(post.date_end).isSame(new Date(), "week"),
        );

      case "THIS MONTH":
        return allPosts.filter(
          (post) =>
            moment(post.date_start).isSame(new Date(), "month") ||
            moment(post.date_end).isSame(new Date(), "month"),
        );

      default:
        if (filters.date_start !== "" && filters.date_end !== "") {
          let date_start = new Date(filters.date_start).toISOString();
          let date_end = new Date(filters.date_end).toISOString();

          return allPosts.filter(
            (post) =>
              // moment(date_start).startOf("day").isBetween((post.date_start, "date"), (post.date_end, "date")) ||
              // moment(date_end).startOf("day").isBetween((post.date_start, "date"), (post.date_end, "date"))
              moment(date_end)
                .startOf("day")
                .isSameOrAfter(post.date_start, "date") &&
              moment(date_start)
                .startOf("day")
                .isSameOrBefore(post.date_end, "date"),
          );
        } else {
          return [];
        }
    }
  }

  let dateFiltered = filterDate();
  let result =
    filters.channel === 0
      ? dateFiltered
      : dateFiltered.filter((post) => post.channel_id === filters.channel);

  try {
    dispatch({
      type: FILTER_POSTS_BY_CHANNEL,
      payload: {
        filters: filters,
        indicator: true,
        posts: result.length === 0 ? [] : result,
        // allPosts.filter(post => channels.includes(post.channel_id))
        // channels.indexOf(post.channel_id) > -1
        //     ? posts.filter(post => channels.includes(post.channel_id))
        //     : posts.filter(post => channels.includes(post.channel_id))
      },
    });
  } catch (error) {
    Promise.reject(error);
  }
};

export const resetAllLoaded = () => async (dispatch) => {
  dispatch({
    type: RESET_ALL_STATE,
    payload: false,
  });
};

export const resetPostFilter = () => async (dispatch) => {
  dispatch({
    type: RESET_POST_FILTER,
  });
};

export const getActionPosts = () => async (dispatch) => {
  const res = await PostService.getActionPosts();

  try {
    dispatch({
      type: GET_ACTION_POSTS,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (error) {
    Promise.reject(error);
  }
};

export const like = (data, userId) => async (dispatch) => {
  try {
    const res = await PostService.likePost(data);
    dispatch(getLikedPostsByUserId(userId));

    dispatch({
      type: LIKE_POST,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (error) {
    Promise.reject(error);
  }
};

export const unlike = (id, userId) => async (dispatch) => {
  try {
    await PostService.unlikePost(id);

    dispatch(getLikedPostsByUserId(userId));
    dispatch({
      type: UNLIKE_POST,
      payload: id,
    });
  } catch (error) {
    console.log(error);
  }
};

export const going = (data, userId) => async (dispatch) => {
  try {
    const res = await PostService.goingPost(data);
    dispatch(getGoingPostsByUserId(userId));

    dispatch({
      type: LIKE_POST,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (error) {
    Promise.reject(error);
  }
};

export const notGoing = (id, userId) => async (dispatch) => {
  try {
    await PostService.unlikePost(id);
    dispatch(getGoingPostsByUserId(userId));

    dispatch({
      type: UNLIKE_POST,
      payload: id,
    });
  } catch (error) {
    console.log(error);
  }
};
