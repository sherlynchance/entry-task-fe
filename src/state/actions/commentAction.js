import { CommentService } from "../../services";
import {
  GET_COMMENTS,
  GET_COMMENTS_BY_POST_ID,
  POST_COMMENT,
} from "../actionTypes/commentActionType";

export const getComments = () => async (dispatch) => {
  try {
    const res = await CommentService.getAll();

    dispatch({
      type: GET_COMMENTS,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const getCommentsByPostId = (id) => async (dispatch) => {
  try {
    const res = await CommentService.getCommentsByPostId(id);

    dispatch({
      type: GET_COMMENTS_BY_POST_ID,
      payload: res.data,
    });
  } catch (error) {
    return Promise.resolve(error);
  }
};

export const postComment = (comment) => async (dispatch) => {
  try {
    const res = await CommentService.postComment(comment);

    dispatch({
      type: POST_COMMENT,
      payload: res.data,
    });
  } catch (error) {
    return Promise.resolve(error);
  }
};
