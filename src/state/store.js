import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducers from "./reducers";

export const store = createStore(
  reducers,
  {}, // default state as 2nd param
  applyMiddleware(thunk), // middleware THUNK as 3rd param
);
