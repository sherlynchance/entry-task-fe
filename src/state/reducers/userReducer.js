import { GET_USERS } from "../actionTypes/userActionType";

const initial_state = [];

const userReducer = (state = initial_state, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_USERS:
      return payload;

    default:
      return state;
  }
};

export default userReducer;
