import { combineReducers } from "redux";
import authReducer from "./authReducer";
import postReducer from "./postReducer";
import channelReducer from "./channelReducer";
import userReducer from "./userReducer";
import commentReducer from "./commentReducer";

export default combineReducers({
  auth: authReducer,
  posts: postReducer,
  channels: channelReducer,
  users: userReducer,
  comments: commentReducer,
});
