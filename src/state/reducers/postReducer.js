import {
  GET_PAGINATED_POSTS,
  GET_POSTS,
  FILTER_POSTS_BY_CHANNEL,
  RESET_ALL_STATE,
  RESET_POST_FILTER,
  LIKE_POST,
  UNLIKE_POST,
  GET_ACTION_POSTS,
  GOING_POST,
  NOT_GOING_POST,
  GET_POST_BY_ID,
  FILTER_LIKED_POSTS_BY_USER,
  FILTER_GOING_POSTS_BY_USER,
  FILTER_PAST_USER_ACTIVITES,
} from "../actionTypes/postActionType";

const initial_state = {
  loading: true,
  posts: [],
  filters: [],
  filteredPosts: [],
  post: [],
  allPosts: [],
  actionPosts: [],
  userActivities: {
    likes: [],
    going: [],
    past: [],
  },
  indicator: false,
};

const postReducer = (state = initial_state, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_POSTS:
      return {
        ...state,
        loading: false,
        allPosts: payload,
      };

    case GET_PAGINATED_POSTS:
      return {
        ...state,
        posts: state.posts.length === 0 ? payload : state.posts.concat(payload),
      };

    case FILTER_LIKED_POSTS_BY_USER:
      return {
        ...state,
        userActivities: {
          ...state.userActivities,
          likes: payload,
        },
      };

    case FILTER_GOING_POSTS_BY_USER:
      return {
        ...state,
        userActivities: {
          ...state.userActivities,
          going: payload,
        },
      };

    case FILTER_PAST_USER_ACTIVITES:
      return {
        ...state,
        userActivities: {
          ...state.userActivities,
          past: payload,
        },
      };

    case FILTER_POSTS_BY_CHANNEL:
      return {
        ...state,
        filters: payload.filters,
        filteredPosts: payload.posts,
        indicator: payload.indicator,
      };

    case GET_POST_BY_ID:
      return {
        ...state,
        loading: payload.loading,
        post: payload.post,
      };

    case RESET_ALL_STATE:
      return initial_state;

    case RESET_POST_FILTER:
      return {
        ...state,
        filteredPosts: initial_state.filteredPosts,
        indicator: payload,
      };

    case GET_ACTION_POSTS:
      return {
        ...state,
        actionPosts: payload,
      };

    case LIKE_POST:
      return {
        ...state,
        actionPosts: [...state.actionPosts, payload],
      };

    case UNLIKE_POST:
      return {
        ...state,
        actionPosts: state.actionPosts.filter(({ id }) => id !== payload),
      };

    case GOING_POST:
      return {
        ...state,
        actionPosts: [...state.actionPosts, payload],
      };

    case NOT_GOING_POST:
      return {
        ...state,
        actionPosts: state.actionPosts.filter(({ id }) => id !== payload),
      };

    default:
      return state;
  }
};

export default postReducer;
