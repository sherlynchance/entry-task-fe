import { GET_CHANNELS } from "../actionTypes/channelActionType";

const initial_state = [];

const channelReducer = (state = initial_state, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_CHANNELS:
      return payload;

    default:
      return state;
  }
};

export default channelReducer;
