import {
  GET_COMMENTS,
  GET_COMMENTS_BY_POST_ID,
  POST_COMMENT,
} from "../actionTypes/commentActionType";

const initial_state = [];

const commentReducer = (state = initial_state, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_COMMENTS:
      return payload;

    case GET_COMMENTS_BY_POST_ID:
      return payload;

    case POST_COMMENT:
      return [...state, payload];

    default:
      return state;
  }
};

export default commentReducer;
