import { SIGN_IN_SUCCESS } from "../actionTypes/authActionType";

const currentUser = JSON.parse(localStorage.getItem("currentUser"));

const initial_state = currentUser
  ? { isLoggedIn: true, currentUser }
  : { isLoggedIn: false, currentUser: null };

const authReducer = (state = initial_state, action) => {
  const { type, payload } = action;

  switch (type) {
    case SIGN_IN_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        currentUser: payload,
      };

    default:
      return state;
  }
};

export default authReducer;
