import React from "react";
import { Route, Switch } from "react-router-dom";
import Login from "../components/Login";
import PostsList from "../components/PostsList";
import PostShow from "../components/PostsList/show";
import Profile from "../components/Profile";
import { PrivateRoute } from "./PrivateRoute";

const routes = () => {
  return (
    <Switch>
      <PrivateRoute exact path="/" component={PostsList} />
      <PrivateRoute exact path="/posts" component={PostsList} />
      <PrivateRoute path={`/posts/:id`} component={PostShow} />
      <PrivateRoute exact path="/profile" component={Profile} />
      <Route exact path="/login" component={Login} />
    </Switch>
  );
};

export default routes;
