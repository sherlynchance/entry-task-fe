import React from "react";
import { Route, Redirect } from "react-router-dom";

export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => {
      const currentUser = localStorage.getItem("currentUser");

      if (!currentUser) {
        return (
          <Redirect
            to={{ pathname: "/login", state: { from: props.location } }}
          />
        );
      } else {
        <Redirect
          to={{ pathname: "/profile", state: { from: props.location } }}
        />;
      }

      return <Component {...props} />;
    }}
  />
);
