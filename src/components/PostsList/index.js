import React, { useRef, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { bindActionCreators } from "redux";
import { channelAction, postAction, userAction } from "../../state";
import NotFound from "../layouts/NotFound";
import { getChannelName } from "../../helpers/utils";
import Post from "./Post";

const PostsList = () => {
  const posts = useSelector((state) => state.posts);
  const channels = useSelector((state) => state.channels);
  const currentUser = useSelector((state) => state.auth.currentUser.user);

  const dispatch = useDispatch();

  const {
    resetPostFilter,
    getPosts,
    changePage,
    resetAllLoaded,
    getActionPosts,
  } = bindActionCreators(postAction, dispatch);
  const { getUsers } = bindActionCreators(userAction, dispatch);
  const { getChannels } = bindActionCreators(channelAction, dispatch);

  const [page, setPage] = useState(0);
  const loader = useRef(null);

  // what happens when SCROLL => update page variable
  const handleObserver = (entities) => {
    const target = entities[0];
    if (target.isIntersecting) setPage((page) => page + 1);
  };

  useEffect(() => {
    resetAllLoaded();
    getActionPosts();
    getUsers();
    getChannels();
    getPosts();

    changePage(page);

    var options = {
      root: null,
      rootMargin: "0px",
      threshold: 1,
    };

    const observer = new IntersectionObserver(handleObserver, options);
    var currentTarget = loader.current;

    if (currentTarget) {
      observer.observe(currentTarget);
    }

    return () => {
      if (currentTarget) {
        observer.unobserve(currentTarget);
      }
    };
  }, []);

  useEffect(() => {
    // if (posts.posts && posts.posts.length != 0)
    if (page > 1) changePage(page);
  }, [page]);

  const refs = useRef();
  refs.current = [];

  function resetFilteredPost() {
    setPage(0);
    resetPostFilter();
  }

  return (
    <div className="pt-5">
      {posts.filteredPosts.length !== 0 && posts.indicator === true ? (
        <>
          <div className="filter-results">
            <h2>{posts.filteredPosts.length} Results</h2>
            <p>
              Searched for&nbsp;
              {posts.filters.channel === 0 ? (
                "All "
              ) : (
                <>{getChannelName(channels, posts.filters.channel)}</>
              )}
              &nbsp;Activities from
              {posts.filters.date_start !== "" &&
              posts.filters.date_end !== "" ? (
                <>
                  {" "}
                  {posts.filters.date_start} / {posts.filters.date_end}
                </>
              ) : (
                <> {posts.filters.date}</>
              )}
            </p>

            <button onClick={() => resetPostFilter()}>CLEAR SEARCH</button>
          </div>

          <Post
            posts={posts?.filteredPosts}
            getChannelName={getChannelName}
            resetAllLoaded={resetAllLoaded}
            currentUser={currentUser.user}
          />
        </>
      ) : (
        <>
          {posts.filteredPosts.length === 0 && posts.indicator === true ? (
            <>
              <div className="filter-results">
                <h2>{posts.filteredPosts.length} Results</h2>
                <p>
                  Searched for&nbsp;
                  {posts.filters.channel === 0 ? (
                    "All "
                  ) : (
                    <>{getChannelName(channels, posts.filters.channel)}</>
                  )}
                  &nbsp;Activities from
                  {posts.filters.date_start !== "" &&
                  posts.filters.date_end !== "" ? (
                    <>
                      {" "}
                      {posts.filters.date_start} / {posts.filters.date_end}
                    </>
                  ) : (
                    <> {posts.filters.date}</>
                  )}
                </p>

                <button onClick={() => resetFilteredPost()}>
                  CLEAR SEARCH
                </button>
              </div>
              <NotFound />
            </>
          ) : (
            <Post
              posts={posts?.posts}
              getChannelName={getChannelName}
              resetAllLoaded={resetAllLoaded}
              currentUser={currentUser.user}
            />
          )}
        </>
      )}

      <div className="loading" ref={loader}>
        {/* <h2>Loading</h2> */}
      </div>
    </div>
  );
};

export default PostsList;
