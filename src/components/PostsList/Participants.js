import React, { useState, useRef, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { bindActionCreators } from "redux";
import { postAction } from "../../state";
import { ReactComponent as LikeOutlineIcon } from "../../assets/icons/like-outline.svg";
import { ReactComponent as CheckOutlineIcon } from "../../assets/icons/check-outline.svg";

const Participants = ({ postDetail }) => {
  const goingButtonRef = useRef();
  const likeButtonRef = useRef();

  const users = useSelector((state) => state.users);
  const actionPosts = useSelector((state) => state.posts.actionPosts);
  const dispatch = useDispatch();

  const { getActionPosts } = bindActionCreators(postAction, dispatch);
  const [loading, setLoading] = useState(true);
  const [indicator, setindicator] = useState({
    goingIndicator: true,
    likeIndicator: true,
  });

  var goingUsers = [];
  var likeUsers = [];
  function getGoingParticipants(type) {
    let actions = actionPosts.filter(
      (action) =>
        action.action_type === (type === "going" ? "going" : "likes") &&
        action.post_id === postDetail.id,
    );

    if (type === "going") {
      goingUsers = actions.map((action) =>
        users.find((user) => user.id === action.user_id),
      );
    } else {
      likeUsers = actions.map((action) =>
        users.find((user) => user.id === action.user_id),
      );
    }
  }

  useEffect(() => {
    getActionPosts().then(() => setLoading(false));
  }, []);

  function toggleGoingDropdown() {
    setindicator({ ...indicator, goingIndicator: !indicator.goingIndicator });
    goingButtonRef.current.className = "d-none";
  }

  function toggleLikeDropdown() {
    setindicator({ ...indicator, likeIndicator: !indicator.likeIndicator });
    likeButtonRef.current.className = "d-none";
  }

  // function checkLength(arr) {
  //     console.log(arr.length)
  //     if (goingButtonRef.current){
  //         if (arr.length > 7) {
  //             arr = arr.slice(0, 6);
  //             // setdata(arr.splice(0, 6));
  //             console.log("sliced", arr)
  //         }
  //         else {
  //             return arr;
  //             // // document.getElementById("down-arrow").style.display = "none";
  //             // goingButtonRef.current.className = "d-none";
  //             // // toggleGoingDropdown()
  //             // console.log(arr)
  //         }
  //         // setdata(sliced)
  //     }

  // }

  // function toggleButton() {
  //     document.getElementById("down-arrow").style.display = "block";
  // }

  if (!loading) {
    getGoingParticipants("likes");
    getGoingParticipants("going");
    // checkLength(arr)
  }

  return (
    <>
      <div className="container">
        <h6 className="m-0 d-flex">
          <CheckOutlineIcon fill={"#AC8EC9"} className="icon" />
          <span>{goingUsers.length} Going</span>
        </h6>
        <div className="participants">
          <div className="icon-container">
            {indicator.goingIndicator ? (
              <>
                {goingUsers.slice(0, 6).map((user) => (
                  <div className="flex-img" key={user.id}>
                    <img src={user.img} alt="Profile" />
                  </div>
                ))}
              </>
            ) : (
              <>
                {goingUsers.map((user) => (
                  <div className="flex-img" key={user.id}>
                    <img src={user.img} alt="Profile" />
                  </div>
                ))}
              </>
            )}

            {goingUsers.length > 7 ? (
              <div
                id="down-arrow"
                className="flex-center"
                ref={goingButtonRef}
                onClick={() => toggleGoingDropdown()}
              >
                <i className="down-arrow"></i>
              </div>
            ) : (
              <></>
            )}
          </div>
        </div>
      </div>

      <div className="container border-top-none">
        <h6 className="m-0 d-flex">
          <LikeOutlineIcon fill={"#AC8EC9"} className="icon" />
          <span>{likeUsers.length} Likes</span>
        </h6>
        <div className="participants">
          <div className="icon-container">
            {indicator.likeIndicator ? (
              <>
                {likeUsers.slice(0, 6).map((user) => (
                  <div className="flex-img" key={user.id}>
                    <img src={user.img} alt="Profile" />
                  </div>
                ))}
              </>
            ) : (
              <>
                {likeUsers.map((user) => (
                  <div className="flex-img" key={user.id}>
                    <img src={user.img} alt="Profile" />
                  </div>
                ))}
              </>
            )}

            {likeUsers.length > 7 ? (
              <div
                id="down-arrow"
                className="flex-center"
                ref={likeButtonRef}
                onClick={() => toggleLikeDropdown()}
              >
                <i className="down-arrow"></i>
              </div>
            ) : (
              <></>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default Participants;
