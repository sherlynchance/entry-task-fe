import React, { useState, useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import {
  channelAction,
  commentAction,
  postAction,
  userAction,
} from "../../state";
import { getChannelName, getUser } from "../../helpers/utils";
import { ReactComponent as InfoOutlineIcon } from "../../assets/icons/info-outline.svg";
import { ReactComponent as PeopleOutlineIcon } from "../../assets/icons/people-outline.svg";
import { ReactComponent as CommentOutlineIcon } from "../../assets/icons/comment-outline.svg";
import { ReactComponent as InfoIcon } from "../../assets/icons/info.svg";
import { ReactComponent as PeopleIcon } from "../../assets/icons/people.svg";
import { ReactComponent as CommentIcon } from "../../assets/icons/comment.svg";
import { ReactComponent as CommentSingleIcon } from "../../assets/icons/comment-single.svg";
import { ReactComponent as LikeOutlineIcon } from "../../assets/icons/like-outline.svg";
import { ReactComponent as LikeIcon } from "../../assets/icons/like.svg";
import { ReactComponent as CheckOutlineIcon } from "../../assets/icons/check-outline.svg";
import { ReactComponent as CheckIcon } from "../../assets/icons/check.svg";
import { ReactComponent as SendIcon } from "../../assets/icons/send.svg";
import { ReactComponent as CrossIcon } from "../../assets/icons/cross.svg";

import Moment from "moment";
import PostDetail from "./PostDetail";
import Participants from "./Participants";
import Comments from "./Comments";

const PostShow = (props) => {
  const { id } = props.match.params;

  const channels = useSelector((state) => state.channels);
  const posts = useSelector((state) => state.posts);
  const users = useSelector((state) => state.users);
  const currentUser = useSelector((state) => state.auth.currentUser.user);
  const { post, loading } = posts;

  const dispatch = useDispatch();
  const { getChannels } = bindActionCreators(channelAction, dispatch);
  const { postComment } = bindActionCreators(commentAction, dispatch);
  const { getUsers } = bindActionCreators(userAction, dispatch);
  const { getPostById, going, like, notGoing, unlike } = bindActionCreators(
    postAction,
    dispatch,
  );

  const moment = new Moment();

  const [placeholder, setPlaceholder] = useState("");

  const commentBoxRef = useRef();
  const [showCommentBox, setShowCommentBox] = useState(false);

  function handleScroll() {
    const sections = document.querySelectorAll("section");
    const navLi = document.querySelectorAll("nav .container ul li");

    // window.addEventListener("scroll", () => {
    let current = "details";
    sections.forEach((section) => {
      const sectionTop = section.offsetTop;
      // const sectionHeight = section.offsetHeight; //clientHeight
      const heightNav = 120; // height of header + sticky-nav
      if (window.scrollY >= sectionTop - heightNav) {
        current = section.getAttribute("id");
      }
    });

    const detailIconActive = document.getElementById("detail-icon-active");
    const detailIconNotActive = document.getElementById(
      "detail-icon-not-active",
    );

    const participantIconActive = document.getElementById(
      "participant-icon-active",
    );
    const participantIconNotActive = document.getElementById(
      "participant-icon-not-active",
    );

    const commentIconActive = document.getElementById("comment-icon-active");
    const commentIconNotActive = document.getElementById(
      "comment-icon-not-active",
    );

    navLi.forEach((li) => {
      li.classList.remove("active");
      if (li.classList.contains(current)) {
        li.classList.add("active");

        if (current === "details") {
          detailIconActive.style.display = "block";
          detailIconNotActive.style.display = "none";

          participantIconActive.style.display = "none";
          participantIconNotActive.style.display = "block";

          commentIconActive.style.display = "none";
          commentIconNotActive.style.display = "block";
        } else if (current === "participants") {
          detailIconActive.style.display = "none";
          detailIconNotActive.style.display = "block";

          participantIconActive.style.display = "block";
          participantIconNotActive.style.display = "none";

          commentIconActive.style.display = "none";
          commentIconNotActive.style.display = "block";
        } else if (current === "comments") {
          detailIconActive.style.display = "none";
          detailIconNotActive.style.display = "block";

          participantIconActive.style.display = "none";
          participantIconNotActive.style.display = "block";

          commentIconActive.style.display = "block";
          commentIconNotActive.style.display = "none";
        } else {
          // setTabIndicator({
          //     detailsTab: false,
          //     participantsTab: false,
          //     commentsTab: false
          // })
          // detailIconActive.style.display = "none";
          // participantIconActive.style.display = "none";
          // detailIconNotActive.style.display = "none";
          // participantIconNotActive.style.display = "none";
          // commentIconNotActive.style.display = "none";
          // commentIconActive.style.display = "none";
          // detailIconNotActive.style.display = "block";
          // participantIconNotActive.style.display = "block";
          // commentIconNotActive.style.display = "block";
        }
      }
    });
    // });
  }

  useEffect(() => {
    getChannels();
    getPostById(id);
    getUsers();

    window.addEventListener("scroll", handleScroll);
  }, []);

  let author = getUser(users, post.user_id);

  // const detailsRef = useRef();
  // const commentsRef = useRef();
  // const participantsRef = useRef();

  function getActionPostId(postId, type) {
    let actionPost = posts.actionPosts.find(
      (action) =>
        action.user_id === currentUser.id &&
        action.post_id === postId &&
        action.action_type === (type === "likes" ? "likes" : "going"),
    );
    return actionPost.id;
  }

  function getActionPost(postId, type) {
    // data to be submitted to ACTION_POSTS
    let data = {
      action_type: type === "likes" ? "likes" : "going",
      user_id: currentUser.id,
      post_id: postId,
    };

    // get all POSTS that is liked by current USER
    let likedPosts = posts.actionPosts.filter(
      (post) =>
        post.user_id === currentUser.id &&
        post.action_type === (type === "likes" ? "likes" : "going"),
    );

    if (
      likedPosts.find(
        (post) => post.post_id === postId && post.user_id === currentUser.id,
      )
    ) {
      return (
        <>
          {type === "likes" ? (
            <LikeIcon
              fill={"#D5EF7F"}
              className="icon"
              onClick={() => unlike(getActionPostId(postId, "likes"))}
            />
          ) : (
            <>
              <span
                onClick={() => notGoing(getActionPostId(postId, "going"))}
                className="icon-check checked d-flex"
              >
                <CheckIcon fill={"#8560A9"} className="icon-going checked" />I
                am going
              </span>
            </>
          )}
        </>
      );
    } else {
      return (
        <>
          {type === "likes" ? (
            <LikeOutlineIcon
              fill={"#453257"}
              className="icon"
              onClick={() => like(data)}
            />
          ) : (
            <>
              <span onClick={() => going(data)} className="icon-check d-flex">
                <CheckOutlineIcon fill={"#D5EF7F"} className="icon-going" />
                Join
              </span>
            </>
          )}
        </>
      );
    }
  }

  function toggleCommentBox() {
    setShowCommentBox(!showCommentBox);
    commentBoxRef.current.focus();
    setPlaceholder("");
  }

  function submitComment() {
    let data = {
      comment: commentBoxRef.current.value,
      created_at: moment.toISOString(),
      post_id: id,
      user_id: currentUser.id,
    };
    postComment(data);
    commentBoxRef.current.value = "";
  }

  function getPlaceholder(placeholder) {
    setPlaceholder(placeholder);
    setShowCommentBox(true);
  }

  return (
    <div className="pt-5">
      {loading === true || channels.length === 0 ? (
        <></>
      ) : (
        <>
          <div className="post-detail">
            <span className="post-channel">
              {getChannelName(channels, post.channel_id)}
            </span>

            <h1 className="mt-15 mb-15">{post.title}</h1>

            <div className="post-author">
              <img src={author?.img} alt="Profile" />
              <div className="content">
                <h3>{author?.username}</h3>
                <span>
                  Published {moment.diff(post.created_at, "days")} days ago
                </span>
              </div>
            </div>
          </div>

          <nav className="sticky-nav">
            <div className="container stats flex-center">
              <ul>
                <li className="stat details active">
                  <a href="#details">
                    <InfoOutlineIcon
                      fill={"#BABABA"}
                      className="stat-img mr-1 d-none"
                      id="detail-icon-not-active"
                    />
                    <InfoIcon
                      fill={"#AECB4F"}
                      className="stat-img mr-1"
                      id="detail-icon-active"
                    />
                    <span>Details</span>
                  </a>
                </li>
                <li className="stat participants">
                  <a href="#participants">
                    <PeopleOutlineIcon
                      fill={"#BABABA"}
                      className="stat-img mr-1"
                      id="participant-icon-not-active"
                    />
                    <PeopleIcon
                      fill={"#AECB4F"}
                      className="stat-img mr-1 d-none"
                      id="participant-icon-active"
                    />
                    <span>Participants</span>
                  </a>
                </li>
                <li className="stat comments">
                  <a href="#comments">
                    <CommentOutlineIcon
                      fill={"#BABABA"}
                      className="stat-img mr-1"
                      id="comment-icon-not-active"
                    />
                    <CommentIcon
                      fill={"#AECB4F !important"}
                      className="stat-img mr-1 d-none"
                      id="comment-icon-active"
                    />
                    <span>Comments</span>
                  </a>
                </li>
              </ul>
            </div>
          </nav>

          <section id="details" className="details-section">
            <PostDetail postDetail={post} />
          </section>

          <section id="participants" className="participants-section mt-0">
            <Participants postDetail={post} />
          </section>

          <section id="comments" className="comments-section mt-0 mb-100">
            <Comments postDetail={post} getPlaceholder={getPlaceholder} />
          </section>

          {/* footer */}
          <div
            className={`post-footer ${showCommentBox === true ? "h-0" : ""}`}
          >
            <div className="like-comment">
              <CommentSingleIcon
                className="icon"
                onClick={() => toggleCommentBox()}
              />
              {getActionPost(Number(id), "likes")}
            </div>

            <div className="going">{getActionPost(Number(id), "going")}</div>
          </div>

          {/* comment input box */}
          <div
            className={`comment-form d-flex ${
              showCommentBox === true ? "h-56" : ""
            }`}
          >
            <div className="input-form flex-center">
              <CrossIcon
                fill={"#D5EF7F"}
                id="close-comment"
                onClick={() => toggleCommentBox()}
              />
              <input
                ref={commentBoxRef}
                type="text"
                placeholder={`${
                  placeholder === ""
                    ? "Leave your comment here"
                    : `@${placeholder}`
                }`}
              />
            </div>
            <button className="submit-button" onClick={() => submitComment()}>
              <SendIcon fill={"#8560A9"} className="icon" />
            </button>
          </div>
        </>
      )}
    </div>
  );
};

export default PostShow;
