import React from "react";
import dateFormat from "dateformat";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import { postAction } from "../../state";
import { ReactComponent as ClockIcon } from "../../assets/icons/time.svg";
import { ReactComponent as CheckIcon } from "../../assets/icons/check.svg";
import { ReactComponent as LikeIcon } from "../../assets/icons/like.svg";
import { ReactComponent as CheckOutlineIcon } from "../../assets/icons/check-outline.svg";
import { ReactComponent as LikeOutlineIcon } from "../../assets/icons/like-outline.svg";
import { getUser } from "../../helpers/utils";

const Post = (props) => {
  const posts = useSelector((state) => state.posts);
  const channels = useSelector((state) => state.channels);
  const currentUser = useSelector((state) => state.auth.currentUser.user);
  const users = useSelector((state) => state.users);

  const dispatch = useDispatch();
  const { going, like, notGoing, unlike } = bindActionCreators(
    postAction,
    dispatch,
  );

  function getActionPostId(postId, type) {
    let actionPost = posts.actionPosts.find(
      (action) =>
        action.user_id === currentUser.id &&
        action.post_id === postId &&
        action.action_type === (type === "likes" ? "likes" : "going"),
    );
    return actionPost.id;
  }

  function getActionPost(postId, type) {
    let data = {
      action_type: type === "likes" ? "likes" : "going",
      user_id: currentUser.id,
      post_id: postId,
    };

    let likedPosts = posts.actionPosts.filter(
      (post) =>
        post.user_id === currentUser.id &&
        post.action_type === (type === "likes" ? "likes" : "going"),
    );

    if (likedPosts.find((post) => post.post_id === postId)) {
      return (
        <li className="liked">
          {type === "likes" ? (
            <>
              <LikeIcon fill={"#FF5C5C"} className="icon" id="like" />
              <span
                onClick={() =>
                  unlike(getActionPostId(postId, "likes"), currentUser.id)
                }
              >
                I like it
              </span>
            </>
          ) : (
            <>
              <CheckIcon fill={"#AECB4F"} className="icon" id="going" />
              <span
                onClick={() =>
                  notGoing(getActionPostId(postId, "going"), currentUser.id)
                }
              >
                I am going!
              </span>
            </>
          )}
        </li>
      );
    } else {
      let totalPostLikes = posts.actionPosts.filter(
        (post) =>
          post.post_id === postId &&
          post.action_type === (type === "likes" ? "likes" : "going"),
      ).length;
      return (
        <li>
          {type === "likes" ? (
            <>
              <LikeOutlineIcon fill={"#AC8EC9"} className="icon" />
              <span onClick={() => like(data, currentUser.id)}>
                {" "}
                {totalPostLikes} Likes
              </span>
            </>
          ) : (
            <>
              <CheckOutlineIcon fill={"#AC8EC9"} className="icon" />
              <span onClick={() => going(data, currentUser.id)}>
                {" "}
                {totalPostLikes} Going
              </span>
            </>
          )}
        </li>
      );
    }
  }

  return (
    <>
      {props.posts.map((post) => (
        <div className="posts-list" key={post.id}>
          <div className="post-author">
            <img
              src={getUser(users, post.user_id)?.img}
              alt="Profile"
              className="profile-img"
            />
            <span>{getUser(users, post.user_id)?.username}</span>
          </div>

          <span className="post-channel">
            {props.getChannelName(channels, post.channel_id)}
          </span>

          <div className="heading-container">
            <div className={`title ${post.img === null ? "" : "maxw-80"}`}>
              <Link
                to={`posts/${post.id}`}
                onClick={() => props.resetAllLoaded()}
              >
                <h1 className="mt-8 mb-8">{post.title}</h1>
              </Link>

              <h6 className="m-0">
                <ClockIcon fill={"#8560A9"} className="icon" />
                {dateFormat(post.date_start, "dd mmmm yyyy HH:MM")} -{" "}
                {dateFormat(post.date_end, "dd mmmm yyyy HH:MM")}
              </h6>
            </div>

            {post.img !== null ? <img src={post.img} alt="Post" /> : <></>}
          </div>

          <p>{post.content}</p>

          <ul className="p-0">
            {getActionPost(post.id, "going")}
            {getActionPost(post.id, "likes")}
          </ul>
        </div>
      ))}
    </>
  );
};

export default Post;
