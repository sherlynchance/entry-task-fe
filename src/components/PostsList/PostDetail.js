import dateFormat from "dateformat";
import React, { useRef, useEffect } from "react";
import { ReactComponent as DateFromIcon } from "../../assets/icons/date-from.svg";
import { ReactComponent as DateToIcon } from "../../assets/icons/date-to.svg";
import Map from "../../assets/img/gmap.png";

const PostDetail = ({ postDetail }) => {
  const contentRef = useRef();

  useEffect(() => {
    let length = contentRef.current.innerText.length;
    if (length > 300) {
      contentRef.current.className = "blur-content";
    }
  }, []);

  function viewAll() {
    contentRef.current.className = "";
  }

  return (
    <>
      <div id="carousel">
        <div className="slide">
          <img src="http://placehold.it/300x150" alt="carousel" />
        </div>
        <div className="slide">
          <img src="http://placehold.it/300x150" alt="carousel" />
        </div>
        <div className="slide">
          <img src="http://placehold.it/300x150" alt="carousel" />
        </div>
        <div className="slide">
          <img src="http://placehold.it/300x150" alt="carousel" />
        </div>
      </div>

      <div className="post-content">
        <p ref={contentRef}>{postDetail.content}</p>
        <button onClick={viewAll}>View all</button>
      </div>

      <div className="post-location">
        <h2 className="heading">When</h2>
        <div className="content d-flex">
          <div id="start">
            <div className="flex-center flex-col">
              <span>
                <DateFromIcon fill={"#D5EF7F"} className="icon" />
                {dateFormat(postDetail.date_start, "dd mmmm yyyy")}
              </span>
              <h4>
                {dateFormat(postDetail.date_start, "h:MM")}
                <small>{dateFormat(postDetail.date_start, "TT")}</small>
              </h4>
            </div>
          </div>
          <div id="end">
            <div className="flex-center flex-col">
              <span>
                <DateToIcon fill={"#D5EF7F"} className="icon" />
                {dateFormat(postDetail?.date_end, "dd mmmm yyyy")}
              </span>
              <h4>
                {dateFormat(postDetail?.date_end, "h:MM")}
                <small>{dateFormat(postDetail?.date_end, "TT")}</small>
              </h4>
            </div>
          </div>
        </div>
      </div>

      <div className="post-location mt-0">
        <h2 className="heading">Where</h2>
        <div className="content">
          <p>
            <b>{postDetail.address}</b>
          </p>
          <p>10 Bayfront Ave, S018956</p>
          <img src={Map} alt="Map" className="map" />
        </div>
      </div>
    </>
  );
};

export default PostDetail;
