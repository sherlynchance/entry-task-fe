import React, { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import { commentAction } from "../../state";
import { ReactComponent as ReplyIcon } from "../../assets/icons/reply.svg";
import { getUser } from "../../helpers/utils";
import moment from "moment";
import Toast from "../layouts/Toast";

const Comments = ({ postDetail, getPlaceholder }) => {
  const toastRef = useRef();

  function inactivityTime() {
    let time;
    window.onload = resetTimer;
    document.onmousemove = resetTimer;
    document.onkeydown = resetTimer;
    document.onscroll = resetTimer;

    function displayToast() {
      if (toastRef.current)
        toastRef.current.className = "toast text-center d-block";
    }

    function resetTimer() {
      if (toastRef.current)
        toastRef.current.className = "toast text-center d-none";

      clearTimeout(time);
      time = setTimeout(displayToast, 10000);
    }
  }

  const comments = useSelector((state) => state.comments);
  const users = useSelector((state) => state.users);
  const dispatch = useDispatch();

  const { getCommentsByPostId } = bindActionCreators(commentAction, dispatch);

  useEffect(() => {
    // window.addEventListener("scroll", inactivityTime);
    getCommentsByPostId(postDetail.id);
  }, []);

  inactivityTime();

  function checkTime(time) {
    let formatDate = moment(time).format("YYYY-MM-DD h:mm:ss a");
    return moment(formatDate).fromNow();
  }

  // inactivityTime();

  const replyComment = (user) => getPlaceholder(user);

  return (
    <>
      <Toast toastRef={toastRef} />

      {comments.map((comment) => (
        <div key={comment.id} className="container d-flex">
          <div className="left">
            <img src={getUser(users, comment.user_id)?.img} alt="Profile" />
          </div>

          <div className="right">
            <h5 className="mt-0">
              {getUser(users, comment.user_id)?.username}
              <span>{checkTime(comment.created_at)}</span>
            </h5>
            <p>{comment.comment}</p>

            <ReplyIcon
              className="reply-icon"
              onClick={() =>
                replyComment(getUser(users, comment.user_id)?.username)
              }
            />
          </div>
        </div>
      ))}
    </>
  );
};

export default Comments;
