import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Router } from "react-router-dom";
import { history } from "../helpers/history";
import Routes from "../routes/routes";
import Header from "./layouts/Header";

const AppLayout = () => {
  const auth = useSelector((state) => state.auth);
  const [sidebarOpen, setSidebarOpen] = useState(false);

  const showSidebar = () => setSidebarOpen(!sidebarOpen);

  return (
    <Router history={history}>
      {auth.isLoggedIn && <Header sidebar={() => showSidebar()} />}

      <div
        id="overlay-sidebar"
        className={`${sidebarOpen === true ? "" : "d-none"}`}
      ></div>
      <main className={`${sidebarOpen === true ? "ml-260" : ""}`}>
        <Routes />
      </main>
    </Router>
  );
};

export default AppLayout;
