import React from "react";

const Toast = (props) => {
  return (
    <div className="toast text-center d-none" ref={props.toastRef}>
      <p>I am a toast :) Slide down and slide up</p>
    </div>
  );
};

export default Toast;
