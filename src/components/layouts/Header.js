import React, { useState } from "react";
import { useLocation } from "react-router";
import { ReactComponent as SearchIcon } from "../../assets/icons/search.svg";
import { ReactComponent as HomeIcon } from "../../assets/icons/home.svg";
import { ReactComponent as CatLogo } from "../../assets/icons/logo-cat.svg";
import Search from "./Search";
import { history } from "../../helpers/history";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const Header = (props) => {
  const { pathname } = useLocation();

  const [showSidebar, setShowSidebar] = useState(false);
  const auth = useSelector((state) => state.auth);
  const { currentUser } = auth;

  function toggleSidebar() {
    let html = document.querySelector("html");
    let body = document.querySelector("body");
    html.style.overflow = showSidebar ? "auto" : "hidden";
    body.style.overflowX = showSidebar ? "unset" : "hidden";
    setShowSidebar(!showSidebar);
    props.sidebar(showSidebar);
  }

  return (
    <div
      id="header"
      className={`${showSidebar === true ? "ml-260 w-500" : ""}`}
    >
      {pathname === "/" ? (
        <>
          <SearchIcon
            fill={"#453257"}
            onClick={() => toggleSidebar()}
            className="ml-16"
          />
          <Search showSidebar={showSidebar} />
        </>
      ) : (
        <HomeIcon
          fill={"#453257"}
          onClick={() => history.push("/")}
          className="ml-16"
        />
      )}
      <CatLogo fill={"#D5EF7F"} />
      <Link to="/profile" className="mr-16">
        <img src={currentUser.user.img} alt="Profile" />
      </Link>
    </div>
  );
};

export default Header;
