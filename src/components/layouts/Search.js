import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { bindActionCreators } from "redux";
import { ReactComponent as SearchIcon } from "../../assets/icons/search.svg";
import { getChannelName } from "../../helpers/utils";
import { channelAction, postAction } from "../../state";
import DateFilter from "./DateFilter";

const Search = ({ showSidebar }) => {
  const channels = useSelector((state) => state.channels);
  const posts = useSelector((state) => state.posts);

  const dispatch = useDispatch();

  const { getChannels } = bindActionCreators(channelAction, dispatch);
  const { filterByChannel } = bindActionCreators(postAction, dispatch);

  // const [selected, setSelected] = useState([]); // tdnya bwt checkbox

  const [filters, setFilters] = useState({
    date: "",
    date_start: "",
    date_end: "",
    channel: null,
  });

  const [customDate, setCustomDate] = useState(false);

  useEffect(() => {
    getChannels();
  }, []);

  // const handleChange = ({target: {checked, value}}) => {
  //     if (checked) {
  //         setSelected([...selected, Number(value)])
  //     }
  //     else {
  //         setSelected(selected.filter(e => e !== Number(value)))
  //     }
  // };

  const handleChannelChange = (e) => {
    setFilters({ ...filters, channel: Number(e.target.value) });
  };

  const handleDateChange = (e) => {
    if (e.target.value === "LATER") {
      setCustomDate(true);
      // handleCustomDateChange;
    } else {
      setCustomDate(false);
    }
    setFilters({ ...filters, date: e.target.value });
  };

  const handleCustomDateChange = (e) => {
    setFilters({
      ...filters,
      [e.target.name]: e.target.value,
    });
  };

  function filterClicked() {
    // filterByChannel(posts.allPosts, selected, filters) // selected bwt checkbox channels
    filterByChannel(posts.allPosts, filters);
  }

  function renderFilterText() {
    if (filters.date === "ANYTIME" && filters.channel === 0) {
      return <small>All activities</small>;
    } else {
      return (
        <small>
          {filters.channel === 0 ? (
            "All "
          ) : (
            <> {getChannelName(channels, filters.channel)} </>
          )}
          activities from
          {customDate === true &&
          filters.date_start !== "" &&
          filters.date_end !== "" ? (
            <>
              {" "}
              {filters.date_start} / {filters.date_end}{" "}
            </>
          ) : (
            <> {filters.date} </>
          )}
        </small>
      );
    }
  }

  return (
    <div
      id="sidebar"
      className={`sidebar ${showSidebar === true ? "show" : ""}`}
    >
      <div className="container">
        <div id="filter-date" className="filter text-center">
          <span className="heading">DATE</span>
          <DateFilter
            customDate={customDate}
            selectedDate={filters}
            handleChange={handleDateChange}
            // innerRef={dateRef}
            handleCustomDateChange={handleCustomDateChange}
          />
        </div>

        <div id="filter-channel" className="filter text-center">
          <span className="heading">CHANNEL</span>
          <ul className="filter-choices mt-10">
            <li>
              <input
                onChange={handleChannelChange}
                type="radio"
                id={0}
                name={"All"}
                value={0}
                checked={filters.channel === 0}
              />
              <label htmlFor={0}>All</label>
            </li>
            {channels.map((channel) => (
              <li key={channel.id}>
                <input
                  type="radio"
                  id={channel.id}
                  name={channel.name}
                  value={channel.id}
                  onChange={handleChannelChange}
                  checked={filters.channel === channel.id}
                />
                <label htmlFor={channel.id}>{channel.name}</label>
              </li>
            ))}
          </ul>
        </div>

        <div className="button-container" onClick={() => filterClicked()}>
          {filters.date && filters.channel !== null ? (
            <button>
              <span className="flex-center">
                <SearchIcon fill={"#453257"} className="icon" />
                SEARCH
              </span>
              {renderFilterText()}
            </button>
          ) : (
            <button className="disabled" disabled={true}>
              <span className="flex-center">
                <SearchIcon fill={"#666666"} className="icon" />
                SEARCH
              </span>
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

export default Search;
