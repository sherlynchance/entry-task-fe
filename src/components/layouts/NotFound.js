import React from "react";
import { ReactComponent as NoActivityIcon } from "../../assets/icons/no-activity.svg";

const NotFound = () => {
  return (
    <div className="no-activity flex-center">
      <NoActivityIcon fill={"#D3C1E5"} className="icon" />
      <span>No activity found</span>
    </div>
  );
};

export default NotFound;
