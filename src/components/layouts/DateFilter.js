import React from "react";
import { ReactComponent as DateFromIcon } from "../../assets/icons/date-from.svg";
import { ReactComponent as DateToIcon } from "../../assets/icons/date-to.svg";

const DateFilter = ({
  handleCustomDateChange,
  customDate,
  selectedDate,
  handleChange,
}) => {
  var dateFilters = [
    { id: 1, name: "ANYTIME" },
    { id: 2, name: "TODAY" },
    { id: 3, name: "TOMORROW" },
    { id: 4, name: "THIS WEEK" },
    { id: 5, name: "THIS MONTH" },
    { id: 6, name: "LATER" },
  ];

  return (
    <>
      <ul className="filter-choices mt-10">
        {dateFilters.map((date) => (
          <li key={date.id}>
            <input
              type="radio"
              id={date.name}
              name={date.name}
              value={date.name}
              onChange={handleChange}
              checked={selectedDate.date === date.name}
            />
            <label htmlFor={date.name}>{date.name}</label>
          </li>
        ))}
      </ul>

      <div
        className={`custom-date flex-center mt-10 ${
          customDate === true ? "expand" : ""
        }`}
      >
        <span className="flex-center">
          <DateFromIcon fill={"#D5EF7F"} className="icon" />
          <input
            type="date"
            onChange={handleCustomDateChange}
            name="date_start"
          ></input>
        </span>
        <small>-</small>
        <span className="flex-center">
          <DateToIcon fill="#D5EF7F" className="icon" />
          <input
            type="date"
            onChange={handleCustomDateChange}
            name="date_end"
          ></input>
        </span>
      </div>
    </>
  );
};

export default DateFilter;
