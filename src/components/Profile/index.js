import React, { useEffect, useState } from "react";
import { ReactComponent as EmailIcon } from "../../assets/icons/email.svg";
import { ReactComponent as CheckIcon } from "../../assets/icons/check.svg";
import { ReactComponent as LikeIcon } from "../../assets/icons/like.svg";
import { ReactComponent as PastIcon } from "../../assets/icons/past.svg";
import { ReactComponent as CheckOutlineIcon } from "../../assets/icons/check-outline.svg";
import { ReactComponent as LikeOutlineIcon } from "../../assets/icons/like-outline.svg";
import { ReactComponent as PastOutlineIcon } from "../../assets/icons/past-outline.svg";
import { useSelector, useDispatch } from "react-redux";
// import { bindActionCreators } from 'redux';
import NotFound from "../layouts/NotFound";
import { bindActionCreators } from "redux";
import { channelAction, postAction, userAction } from "../../state";
import Post from "../PostsList/Post";
import { getChannelName } from "../../helpers/utils";

const Profile = () => {
  const auth = useSelector((state) => state.auth);
  const { isLoggedIn, currentUser } = auth;
  const posts = useSelector((state) => state.posts);

  const dispatch = useDispatch();

  const {
    getPastUserActivities,
    getGoingPostsByUserId,
    getLikedPostsByUserId,
    getPosts,
    resetAllLoaded,
    getActionPosts,
  } = bindActionCreators(postAction, dispatch);
  const { getUsers } = bindActionCreators(userAction, dispatch);
  const { getChannels } = bindActionCreators(channelAction, dispatch);

  const [tab, setTab] = useState("likes");

  useEffect(() => {
    resetAllLoaded();
    getGoingPostsByUserId(currentUser.user.id);
    getLikedPostsByUserId(currentUser.user.id);
    getPastUserActivities(currentUser.user.id);
    getPosts();
    getActionPosts();
    getUsers();
    getChannels();

    document.querySelector("main").classList.add("body-100");

    return () => {
      document.querySelector("main").classList.remove("body-100");
    };
  }, []);

  const handleTabChange = (e) => setTab(e.target.id);

  return (
    <>
      {isLoggedIn === true && !posts.loading ? (
        <>
          <div className="container-flex">
            <div className="profile">
              <img src={currentUser.user.img} alt={"Profile Image"} />
              <h1 className="mb-0">{currentUser.user.username}</h1>

              <h6 className="flex-center">
                <span>
                  <EmailIcon fill={"#8560A9"} />
                </span>
                {currentUser.user.email}
              </h6>

              <div className="stats flex-center">
                <div className="stat flex-center">
                  {tab !== "likes" ? (
                    <LikeOutlineIcon
                      fill={"#BABABA"}
                      className="stat-img mr-1"
                    />
                  ) : (
                    <LikeIcon fill={"#AECB4F"} className="stat-img mr-1" />
                  )}
                  <span
                    onClick={handleTabChange}
                    id="likes"
                    className={`${tab === "likes" ? "active" : ""}`}
                  >
                    {posts.userActivities.likes.length} Likes
                  </span>
                </div>
                <div className="stat flex-center">
                  {tab !== "going" ? (
                    <CheckOutlineIcon
                      fill={"#BABABA"}
                      className="stat-img mr-1"
                    />
                  ) : (
                    <CheckIcon fill={"#AECB4F"} className="stat-img mr-1" />
                  )}
                  <span
                    onClick={handleTabChange}
                    id="going"
                    className={`${tab === "going" ? "active" : ""}`}
                  >
                    {posts.userActivities.going.length} Going
                  </span>
                </div>
                <div className="stat flex-center" id="past-stat">
                  {tab !== "past" ? (
                    <PastOutlineIcon
                      fill={"#BABABA"}
                      className="stat-img mr-1"
                    />
                  ) : (
                    <PastIcon fill={"#AECB4F"} className="stat-img mr-1" />
                  )}
                  <span
                    onClick={handleTabChange}
                    id="past"
                    className={`${tab === "past" ? "active" : ""}`}
                  >
                    {posts.userActivities.past.length} Past
                  </span>
                </div>
              </div>
            </div>

            <div className="content-container">
              {tab === "likes" ? (
                <>
                  {posts.userActivities.likes.length !== 0 ? (
                    <Post
                      posts={posts?.userActivities.likes}
                      // getUserDetail={getUserDetail}
                      getChannelName={getChannelName}
                      // getActionPost={getActionPost}
                      resetAllLoaded={resetAllLoaded}
                      currentUser={currentUser.user}
                    />
                  ) : (
                    <NotFound />
                  )}
                </>
              ) : tab === "going" ? (
                <>
                  {posts.userActivities.going.length !== 0 ? (
                    <Post
                      posts={posts?.userActivities.going}
                      getChannelName={getChannelName}
                      resetAllLoaded={resetAllLoaded}
                      currentUser={currentUser.user}
                    />
                  ) : (
                    <NotFound />
                  )}
                </>
              ) : (
                <>
                  {posts.userActivities.past.length !== 0 ? (
                    <Post
                      posts={posts?.userActivities.past}
                      getChannelName={getChannelName}
                      resetAllLoaded={resetAllLoaded}
                      currentUser={currentUser.user}
                      hideLikeGoing={true}
                    />
                  ) : (
                    <NotFound />
                  )}
                </>
              )}
            </div>
          </div>
        </>
      ) : (
        <></>
      )}
    </>
  );
};

export default Profile;
