import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { bindActionCreators } from "redux";
import { useForm } from "react-hook-form";
// import { yupResolver } from "@hookform/resolvers/yup";
// import * as Yup from "yup";
import { ReactComponent as CatLogo } from "../../assets/icons/logo-cat.svg";
import { ReactComponent as UserIcon } from "../../assets/icons/user.svg";
import { ReactComponent as PassIcon } from "../../assets/icons/password.svg";
import { authAction } from "../../state";

const Login = (props) => {
  useEffect(() => {
    document.querySelector("main").classList.add("mt-0");
  }, []);
  // // form validation
  // const validationSchema = Yup.object().shape({
  //   email: Yup.string().required("Email harus diisi"),
  //   password: Yup.string().required("Kata Sandi harus diisi"),
  // });

  const { register, handleSubmit, reset, formState } = useForm({
    // resolver: yupResolver(validationSchema),
  });

  const dispatch = useDispatch();

  const auth = useSelector((state) => state.auth);
  const { sign_in } = bindActionCreators(authAction, dispatch);

  if (auth.isLoggedIn) {
    props.history.push("/");
  }

  function login(data) {
    sign_in(data);
    props.history.push("/profile");
  }

  return (
    <>
      <div className="overlay"></div>
      <div id="login-bg">
        <div className="login-container">
          <div className="login-content text-center">
            <h2>FIND THE MOST LOVED ACTIVITIES</h2>
            <h1>BLACK CAT</h1>
            <span>
              <CatLogo fill={"#D5EF7F"} className="logo-img" />
            </span>
          </div>
          <form
            onSubmit={handleSubmit(login)}
            onReset={reset}
            className="mb-70"
          >
            <div className="form-group login-field">
              <UserIcon fill={"#D3C1E5"} className="field-icon" />
              <input
                name="email"
                type="text"
                placeholder="Email"
                {...register("email", { required: true })}
                // className={`form-control ${errors.email ? 'is-invalid' : ''}`}
              />
              {/* <div className="invalid-feedback">{errors.email?.message}</div> */}
            </div>

            <div className="form-group login-field">
              <PassIcon fill={"#D3C1E5"} className="field-icon" />
              <input
                name="password"
                type="password"
                placeholder="Password"
                {...register("password", { required: true })}
                // className={`form-control ${errors.email ? 'is-invalid' : ''}`}
              />
            </div>

            <button type="submit" disabled={formState.isSubmitting}>
              {formState.isSubmitting && (
                <span className="spinner-border spinner-border-sm mr-1"></span>
              )}
              SIGN IN
            </button>
          </form>
        </div>
      </div>
    </>
  );
};

export default Login;
