# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### `Dummy API`

get all USERS who likes the POST
GET   /posts/1/likes

get all posts created by a specific user (user_id)
GET http://localhost:8000/users/8/posts

get all posts with a specific channel (channel_id)
GET http://localhost:8000/channels/1/posts

LIKE & UNLIKE POST
GET /post_actions?action_type=likes
GET /post_actions?action_type=going
POST /post_actions
DELETE /post_actions/1 
(where post_id and user_id -> logged in)


Infinite scrolling
- intersection observer
- call every 10 post


DB Schema

Users
1. id
2. email
3. username
4. password

Channels
1. id
2. name

Posts
1. id
2. title
3. content
4. date_start
5. data_end
6. published_at
7. address
8. address_detail
9. user_id -> USERS FK
10. channel_id -> CHANNEL FK

Comments
1. id
2. comment
3. createt_at
4. user_id -> USERS FK
5. post_id -> POSTS FK


Post_actions
1. id
2. action_type -> LIKES or GOING
3. post_id
4. user_id


### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
